FROM python:3.7

RUN apt update && apt -y upgrade

RUN pip install mkdocs mkdocs-material pylint pytest
